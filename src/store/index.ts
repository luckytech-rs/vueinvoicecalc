import { createStore } from "vuex";

import { items } from "./modules/items";

export default createStore({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    items,
  },
});
