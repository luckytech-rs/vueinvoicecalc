/* eslint-disable */

const initialState = () => {
  const ls = JSON.parse(<string>localStorage.getItem("INITIAL_STATE"));
  return ls && ls.items && ls.items.length ? ls.items : [];
};

export const items = {
  state: () => ({
    items: initialState(),
  }),
  actions: {
    addItem(context: any, payload: any) {
      context.commit("NEW_ITEM", payload);
      localStorage.setItem("INITIAL_STATE", JSON.stringify(context.state));
    },
    deleteItems(context: any, ids: any) {
      console.log(ids);
      ids.map((id: number) => {
        console.log(id);
        context.commit("REMOVE_ITEM", id);
        localStorage.setItem("INITIAL_STATE", JSON.stringify(context.state));
      });
    },
  },
  mutations: {
    NEW_ITEM(state: { items: any[] }, item: any) {
      state.items.push(item);
    },
    REMOVE_ITEM(state: { items: any[] }, id: number) {
      state.items = state.items.filter((item) => {
        return id !== item.id;
      });
    },
  },
  getters: {
    items: (state: any) => state.items,
  },
};
